# PROJECT
Projects that have been done
## Mobile
- [SIAK Mobile - Flutter](https://github.com/fauzywijaya/siak-mobile)
- [Fic'O Fit - Android Native - Capstone Project](https://github.com/Fic-o-Fit/ficofit-android)
- [Capture Moment - Android Native](https://github.com/fauzywijaya/capture-moment)
- [Walleto - Flutter - Capstone Project](https://github.com/Alvin04072001/walleto)
- [Restaurant App - Flutter](https://github.com/fauzywijaya/restaurant-app)

## Figma
- [UI UX SIAK Mobile](https://www.figma.com/file/1WUGTdiUsigcasW6WdKYwz/SIAK-V1?node-id=0%3A1&t=EpNOYm4Ro9eAfOwf-1)
- [UI UX MoviePlex](https://www.figma.com/file/iZQTbNG6KdRQ5NVuqkYnKY/MoviPlex?node-id=0%3A1&t=Cm1E4R1g9GqDNd30-1) 